package com.epam;
/**
 * Represents a hamster class..
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class Hamster extends Animal  {
    /**
     * Constructure.
     *
     * @param name hamster`s name.
     * @param color hamster`s color.
     */
    public Hamster(String name, String color) {
        super(name, color);
    }
}
