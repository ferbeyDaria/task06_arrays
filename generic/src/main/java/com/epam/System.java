package com.epam;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a generic class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class System<T> {
    private List<? super T> system = new ArrayList<>();

    /**
     * Add element to queue.
     *
     * @param element
     */
    public void addElement(T element) {
        system.add(element);
    }

    /**
     * Get element.
     *
     * @param index
     * @return position in array.
     */
    public T getElement(int index) {
        return (T) system.get(index);
    }

    /**
     * Get age animals.
     *
     * @return size
     */
    public int getAge() {
        return system.size();
    }
}
