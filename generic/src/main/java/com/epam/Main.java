package com.epam;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Queue;

/**
 * Represents an comparator price of holidays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Main main = new Main();
        System<Animal> system = new System<>();
        system.addElement(new Cat("Mars", "white"));
        system.addElement(new Dog("Boby", "brown"));
        system.addElement(new Hamster("Bulochka", "rude"));
        for (int i = 0; i < system.getAge(); i++) {
            logger.info(system.getElement(i));
        }

        Queue<Integer> myPriorityQueue = new PriorityQueue<Integer>((o1, o2) -> o2 - o1);
        myPriorityQueue.add(7);
        myPriorityQueue.add(12);
        myPriorityQueue.add(36);
        myPriorityQueue.add(45);
        myPriorityQueue.add(68);
        myPriorityQueue.add(7);
        myPriorityQueue.add(36);

        while (!myPriorityQueue.isEmpty()) {
            logger.info(myPriorityQueue.poll());
        }


    }
}
