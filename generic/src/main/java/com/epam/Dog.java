package com.epam;

/**
 * Represents a dog class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class Dog extends Animal {
    /**
     * Constructure.
     *
     * @param name dog`s name.
     * @param color dog`s color.
     */
    public Dog(String name, String color) {
        super(name, color);
    }
}
