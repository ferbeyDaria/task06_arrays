package com.epam;

import java.util.*;

/**
 * Represents a my priority queue.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class PriorityQueue<T> implements Queue {
    List<T> queue;
    Comparator<T> comparator;

    PriorityQueue(Comparator<T> comparator) {
        queue = new ArrayList<>();
        this.comparator = comparator;
    }

    /**
     * Find head number in queue.
     *
     * @return head number
     */
    private int findHeadNumber() {
        int headNumber = 0;
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(queue.get(i), queue.get(headNumber)) > 0) {
                headNumber = i;
            }
        }
        return headNumber;
    }

    /**
     * Add element to queue.
     *
     * @param o specific element to  be entered.
     * @return true if element added to queue.
     */
    @Override
    public boolean add(Object o) {
        if (queue.add((T) o)) return true;
        else return false;
    }

    /**
     * Remove  a specific object from the queue.
     *
     * @param o specific element to  be entered.
     * @return true if element removed.
     */
    @Override
    public boolean remove(Object o) {
        return queue.remove(o);
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    /**
     * Size of queue.
     *
     * @return no. of elements.
     */

    @Override
    public int size() {
        return queue.size();
    }

    /**
     * Identifies the head element of the queue.
     *
     * @return calls if head exists, else null
     */
    @Override
    public Object peek() {
        return queue.get(findHeadNumber());
    }

    /**
     * Identifies the head and then removes it.
     *
     * @return calls if head exists, else null.
     */
    @Override
    public Object poll() {
        if (size() == 0) {
            return null;
        }
        int headNumber = findHeadNumber();
        Object obj = queue.get(headNumber);
        queue.remove(headNumber);
        return obj;
    }

    @Override
    public Object element() {
        return null;
    }

    /**
     * Offer is required to insert a specific element to the given priority queue.
     *
     * @param o specific element to  be entered.
     * @return call return true.
     */

    @Override
    public boolean offer(Object o) {
        return true;
    }

    @Override
    public Object remove() {
        return null;
    }

    /**
     * Check is empty.
     *
     * @return true if size isn`t null.
     */
    @Override
    public boolean isEmpty() {
        if (size() == 0) return true;
        else return false;
    }

    /**
     * Returns true if the priority queue contains the element “o”.
     *
     * @param o specific element to  be entered.
     * @return true if the priority queue contains the element “o”.
     */

    @Override
    public boolean contains(Object o) {
        return false;
    }

    /**
     * Itereates over the queue element.
     *
     * @return calls iterator over the elements in the queue.
     */
    @Override
    public Iterator iterator() {
        return null;
    }

    /**
     * Returns an array containing the elements of PriorityQueue.
     *
     * @return an array containing all the elements of PriorityQueue.
     */
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    /**
     * Returns the array having the elements of the Priority Queue.
     *
     * @param a specific element to  be entered.
     * @return call an array containing all the elements of the array.
     */
    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    /**
     * Clears all the elements of the PriorityQueue
     */
    @Override
    public void clear() {

    }

}
