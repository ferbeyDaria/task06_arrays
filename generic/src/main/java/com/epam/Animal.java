package com.epam;

/**
 * Represents an animal class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class Animal {
    /**
     * Animal`s name.
     */
    protected String name;
    /**
     * Animal`s color.
     */
    protected String color;

    /**
     * Constracture.
     *
     * @param name  animal`s name
     * @param color animal`s color.
     */
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    /**
     * Print array to string.
     *
     * @return array to string.
     */
    @Override
    public String toString() {
        return "name='" + name + '\n' +
                ", color='" + color + '\n';
    }
}
