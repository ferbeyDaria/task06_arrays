package com.epam;

/**
 * Represents a cat class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-21-10
 */
public class Cat extends Animal {
    /**
     * Constructure.
     *
     * @param name  cat`s name.
     * @param color cat`s color.
     */
    public Cat(String name, String color) {
        super(name, color);
    }
}
