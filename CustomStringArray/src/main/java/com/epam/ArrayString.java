package com.epam;

/**
 * Represents an array of string class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-25-10
 */
public class ArrayString {
    private String[] arr;
    private int length;
    private int size;

    public ArrayString() {
        size = 2;
        length = 0;
        arr = new String[size];
    }

    /**
     * If the internal array isn’t big enough for the next add,
     * array automatically resize
     */
    public void resize() {
        size += size;
        String[] newStringArray = new String[size];
        for (int i = 0; i < arr.length; i++) {
            newStringArray[i] = arr[i];
        }
        arr = newStringArray;
    }

    /**
     * Added element to array.
     *
     * @param obj element
     */
    public void add(String obj) {
        if (length == size) {
            resize();
        }
        arr[length] = obj;
        length++;
    }

    /**
     * Geted element from array.
     *
     * @param index index of element
     * @return
     */
    public String get(int index) {
        return arr[index];
    }

}
