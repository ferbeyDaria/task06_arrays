package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
/**
 * Represents an array of string class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-25-10
 */
public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        ArrayString str = new ArrayString();
        List<String> list = new ArrayList<>(5);
        list.add("Bob");
        list.add("Lola");
        list.add("Bim");
        list.add("Ann");
        list.add("Job");
        logger.info(list);

        str.add("Olya");
        str.add("Bodya");
        str.add("Pavlo");
        str.add("Iryna");
        str.add("Nastya");
        logger.info(str);

    }

}
