package com.epam;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents an array class.
 * Find the elements present in both arrays.
 * Find the elements present in one of the arrays.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-23-10
 */
public class Array {
    private List<Integer> listFirst;
    private List<Integer> listSecond;
    private List<Integer> listThird;
    Random random = new Random();

    public Array() {
        listFirst = new ArrayList<Integer>();
        listSecond = new ArrayList<Integer>();
        listThird = new ArrayList<Integer>();

        for (int i = 0; i < 5; i++) {
//            listFirst.add(random.nextInt());
            listFirst.add(i);
        }
        for (int i = 2; i < 7; i++) {
            listSecond.add(i);
        }
        for (int i = 0; i < 7; i++) {
            listThird.add(random.nextInt(5));
        }
    }

    /**
     * Find the elements present in both arrays.
     *
     * @return array of recurring elements.
     */
    public List<Integer> recurringElements() {
        List<Integer> result = new ArrayList<Integer>();

        for (Integer i : listFirst) {
            for (Integer j : listSecond) {
                if (i.equals(j) == true) {
                    result.add(i);
                }
            }
        }
        return result;
    }

    /**
     * Merge two arrays.
     *
     * @return array.
     */
    public List<Integer> mergeTwoLists() {
        List<Integer> result = Stream.concat(listFirst.stream(), listSecond.stream()).collect(Collectors.toList());
        return result;
    }

    /**
     * Find the elements present in one of the arrays.
     *
     * @return arrays of unique elements.
     */
    public List<Integer> uniqueElemnts() {
        List<Integer> result = mergeTwoLists();
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < listFirst.size(); j++) {
                for (int k = 0; k < listSecond.size(); k++) {
                    if (result.get(i) == listFirst.get(j) && result.get(i) == listSecond.get(k)) {
                        result.remove(i);
                    }
                }
            }
        }
        return result;
    }

    /**
     * The method that convert array to string.
     *
     * @return array to string.
     */
    @Override
    public String toString() {
        return "listFirst: " + listFirst + "\n" +
                "listSecond: " + listSecond + "\n" +
                "litThird: " + listThird;
    }
}
