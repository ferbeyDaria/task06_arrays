package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Represents a main class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-23-10
 */
public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Array arr = new Array();
        System.out.println(arr);
        //the elements present in both arrays,
        System.out.println(arr.recurringElements());
        //the elements present in of the arrays.
        System.out.println(arr.uniqueElemnts());

        logger.info(arr.toString());
        logger.info("The elements present in both arrays" + arr.recurringElements());
        logger.info("The elements present in both arrays" + arr.uniqueElemnts());

    }

    @Override
    public String toString() {
        return "";
    }
}
