package com.epam;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an array class.
 * Deleting duplicate items more than 2 times.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-23-10
 */
public class Array {
    List<Integer> list;

    public Array() {
        list = new ArrayList<Integer>();
        /**
         * Fiiling the array with random value.
         */
        for (int i = 0; i < 10; i++) {
            list.add((int) (Math.random() * 10));
        }

    }

    /**
     * Find how many times the items are repeated.
     *
     * @param element element of array
     * @return number of repetitions
     */
    private int isRepeatedElement(int element) {
        int counter = 0;
        for (Integer i : list) {
            if (i == element) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Removed elements that are repeated more than 2 times.
     *
     * @return array of number.
     */
    public List<Integer> removeRepeatedElemnts() {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < list.size(); i++) {
            if (!(isRepeatedElement(list.get(i)) > 2)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    /**
     * Convert array to string.
     *
     * @return string
     */
    @Override
    public String toString() {
        return "list=" + list;
    }
}
